import { StyleSheet } from 'react-native'

//#335e9e
export default StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: '#ffffff',
        justifyContent: 'center',
        alignItems: 'center'
    }
})