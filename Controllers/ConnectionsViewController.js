import React from 'react'
import { View, StyleSheet, Text } from 'react-native'
import GlobalStyleSheet from '../GlobalStyleSheet/GlobalStyleSheet'

const ConnectionsViewController = (props) => {
    return (
        <View style={GlobalStyleSheet.screen}>
            <Text>
                Connections Screen
            </Text>
        </View>
    )
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: '#ffffff',
        justifyContent: 'center',
        alignItems: 'center'
    }
})

export default ConnectionsViewController