import React, { useEffect, useState } from 'react'
import { StyleSheet, View, Text, TouchableOpacity, SectionList } from 'react-native'

import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import { StatusBar } from 'expo-status-bar';
import Constant from 'expo-constants'
const SettingsViewController = (props) => {

    const settingsArray = [
        {
            title: "Security",
            data: [[
                type = 'Backup Wallet',
                logo = 'wallet',
            ], [
                type = 'Change Access PIN',
                logo = 'lock-open',
            ]]
        },
        {
            title: "Preferences",
            data: [[
                type = 'Network',
                logo = 'project-diagram',
            ], [
                type = 'Notification',
                logo = 'bell-slash',
            ]]
        }
        ,
        {
            title: "Privacy",
            data: [[
                type = 'Wallet Privacy',
                logo = 'user-secret',
            ]]
        }
        ,
        {
            title: "Support",
            data: [[
                type = 'Contsct Us',
                logo = 'id-card-alt',
            ], [
                type = 'Logout',
                logo = 'sign-out-alt',
            ]]
        }
    ];

    const [data, setData] = useState(settingsArray)

    return (
        <View style={styles.container}>
            <View style={{ flex: 0.10, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                <TouchableOpacity
                    style={{ flex: 0.10, alignItems: 'center', justifyContent: 'center' }}
                    onPress={() => props.navigation.goBack()}>
                    <FontAwesome5 name={'caret-left'} color={'#335e9e'} size={30} />
                </TouchableOpacity>

                <View style={{ flex: 0.80, alignItems: 'center', justifyContent: 'center' }}>
                    <Text style={{
                        fontSize: 32,
                        color: '#00000090'
                    }}>{'Settings'}</Text>
                </View>

                <TouchableOpacity
                    style={{ flex: 0.10 }}>
                </TouchableOpacity>


            </View>
            <View style={{ flex: 0.90, justifyContent: 'center', alignItems: 'center' }}>


                <SectionList style={{ width: '90%' }}
                    sections={data}
                    keyExtractor={(item, index) => index.toString()}
                    renderSectionHeader={({ section }) => (
                        <View style={{ justifyContent: 'center', alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between' }}>
                            <Text style={styles.header}>{section.title}</Text>

                            <TouchableOpacity onPress={() => {
                                section.data.pop()
                                setData(data)
                            }}>
                            </TouchableOpacity>

                        </View>
                    )}
                    renderItem={({ item, index }) => {

                        return (
                            <TouchableOpacity style={{ ...styles.item, flexDirection: 'row' }} >

                                <View style={{ flex: 0.15 }}>
                                    <FontAwesome5 name={item[1]} color={'#335e9e'} size={28} />
                                </View>

                                <View style={{ flex: 0.85, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                                    <Text style={styles.cardTittle}>{item[0]}</Text>
                                    <FontAwesome5 name={'chevron-right'} color={'#335e9e'} size={15} />
                                </View>


                            </TouchableOpacity>
                        )
                    }}
                // 
                />
            </View>
            <StatusBar style="auto" />
        </View>
    );
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: Constant.statusBarHeight + 10,
        backgroundColor: '#fff',
        // alignItems: 'center',
    },
    item: {
        padding: 5,
        marginVertical: 8,
        height: 50,
        justifyContent: 'flex-start',
        alignItems: 'center',
        marginLeft: 15
    },
    header: {
        fontSize: 30,
        margin: 5,
        color: '#00000090',
        backgroundColor: 'white'
    },
    cardTittle: {
        color: 'grey',
        fontSize: 22,
    }

});



export default SettingsViewController

