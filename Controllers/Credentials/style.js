import { StyleSheet } from 'react-native';
import Constant from 'expo-constants'


const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: Constant.statusBarHeight + 20,
        backgroundColor: '#fff',
        alignItems: 'center',
    },
    listSize: {
        width: '90%'
    },
    sectionHeader: {
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: 'white'
    },
    headerTittle: {
        fontSize: 32,
        backgroundColor: "#fff",
    },
    finalArrayBtn: {
        fontSize: 22,
        backgroundColor: "orange",
        padding: 10,
        marginBottom: 20,
        borderRadius: 10
    },
    addLogo: {
        width: 50,
        height: 50,
    },
    minusLogo: {
        width: 30,
        height: 30,
    }

});

export default styles