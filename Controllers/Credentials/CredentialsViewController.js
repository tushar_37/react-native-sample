import { StatusBar } from 'expo-status-bar';
import React, { useState } from 'react';
import { SectionList, Text, TouchableOpacity, View, } from 'react-native';
import CredentialCardView from '../../childComponent/CredentialCardView';

import styles from './style'
const CredentialsViewController = (props) => {


    const didSelectItem = (item) => {
        console.log('item', item)
    }


    const SectionArray = [
        {
            title: "Credebl",
            data: [[
                type = 'Login Credential',
                credId = 'as78-45jj-koy4-1111',
            ], [
                type = 'Admin Credential',
                credId = 'as78-45jj-koy4-2222',
            ], [
                type = 'HR Credential',
                credId = 'as78-45jj-koy4-3333',
            ]]
        },
        {
            title: "HDFC Bank",
            data: [[
                type = 'Manager',
                credId = 'asdf-koo4-1111-2317',
            ], [
                type = 'Admin',
                credId = 'asdf-koo4-1212-2317',
            ], [
                type = 'Staff Section',
                credId = 'asdf-koo4-2323-2317',
            ]]
        }
    ];

    const [data, setData] = useState(SectionArray)

    const SectionHeader = ({ sectionTittle }) => {
        return (
            <View style={styles.sectionHeader}>
                <Text style={styles.headerTittle}>{sectionTittle}</Text>
            </View>
        )
    }


    return (
        <View style={styles.container}>
            <SectionList
                style={styles.listSize}
                sections={data}
                keyExtractor={(item, index) => index.toString()}
                renderSectionHeader={({ section }) => (
                    <SectionHeader sectionTittle={section.title} />
                )}
                renderItem={({ item, index }) => {
                    return (
                        <CredentialCardView
                            tittle={item[0]}
                            subTittle={item[1]}
                            onPress={() => didSelectItem(item)}
                        />
                    )
                }}
            />
            <StatusBar style="auto" />
        </View>
    );
}


export default CredentialsViewController
