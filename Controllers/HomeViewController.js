import React from 'react'
import { View, StyleSheet, Text, Image, TouchableOpacity, Modal } from 'react-native'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import Constants from 'expo-constants';
import { RFValue } from "react-native-responsive-fontsize";

const HomeViewController = (props) => {

    const goToCredentialTab = () => {
        props.navigation.navigate('Credentials')
    }

    const goToConnectioTab = () => {
        props.navigation.navigate('Connections')
    }

    const goToSettingsTab = () => {
        props.navigation.navigate('Settings')
    }

    return (
        <View style={styles.screen}>

            <View style={styles.workingBox}>
                <View style={{ flex: 0.10, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>

                    <View style={{ flex: 0.80, justifyContent: 'flex-start' }}>
                        <Image style={{ width: '60%', height: '50%' }} source={require('../assets/adeyaLineLogo.png')} />
                    </View>

                    <TouchableOpacity
                        style={{ flex: 0.10, justifyContent: 'flex-end' }}>
                        <FontAwesome5 name={'bell'} color={'#335e9e'} size={25} />
                    </TouchableOpacity>


                    <TouchableOpacity
                        onPress={goToSettingsTab}
                        style={{ flex: 0.10, justifyContent: 'flex-end' }}>
                        <FontAwesome5 name={'cog'} color={'#335e9e'} size={25} />
                    </TouchableOpacity>

                </View>

                <View style={{ flex: 0.10, justifyContent: 'center', paddingLeft: 10 }}>
                    <Text style={styles.tittle}>In your wallet...</Text>
                </View>

                <TouchableOpacity
                    style={styles.detailCard}
                    onPress={goToConnectioTab}>
                    <Text style={styles.cardInfo} >Connection</Text>
                    <Text style={styles.cardInfo} >02</Text>
                </TouchableOpacity>

                <TouchableOpacity
                    style={styles.detailCard}
                    onPress={goToCredentialTab}>
                    <Text style={styles.cardInfo} >Credentials</Text>
                    <Text style={styles.cardInfo} >03</Text>
                </TouchableOpacity>

                <TouchableOpacity style={styles.detailCard}>
                    <Text style={styles.cardInfo} >Proof Request</Text>
                    <Text style={styles.cardInfo} >01</Text>
                </TouchableOpacity>

                <View style={{ flex: 0.64, flexDirection: 'column', justifyContent: 'space-between' }}>

                    <View style={{ flex: 0.50, flexDirection: 'row-reverse', backgroundColor: '#ebeff5', borderRadius: 10, margin: 5 }}>

                        <Image style={{ width: '50%', height: '90%', borderRadius: 10, margin: 10, justifyContent: 'center' }} source={require('../assets/ShowQR.jpg')} />

                        <TouchableOpacity style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                            <View style={styles.activityCard}>
                                <Text style={{ fontSize: 24, color: 'white' }}>Show QR code</Text>
                            </View>
                        </TouchableOpacity>

                    </View>

                    <View style={{ flex: 0.50, flexDirection: 'row', backgroundColor: '#ebeff5', borderRadius: 10, margin: 5 }}>

                        <Image style={{ width: '50%', height: '90%', borderRadius: 10, margin: 10, justifyContent: 'center' }} source={require('../assets/ScanQR.jpg')} />

                        <TouchableOpacity style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                            <View style={styles.activityCard}>
                                <Text style={{ fontSize: 24, color: 'white' }}>Scan QR code</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        </View >
    )
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center'
    },
    workingBox: {
        flex: 1,
        marginTop: Constants.statusBarHeight,
        width: '95%',
        backgroundColor: '#ffffff',
        borderRadius: 10,
    },
    detailCard: {
        padding: 10, margin: 5, flex: 0.06, flexDirection: 'row', backgroundColor: '#597fbd', borderRadius: 30, justifyContent: 'space-between', alignItems: 'center'
    },
    activityCard: { backgroundColor: '#597fbd', padding: 10, fontSize: 24, color: 'white', borderRadius: 10, justifyContent: 'center', alignItems: 'center' },
    tittle: {
        fontSize: RFValue(30),
    },
    cardInfo: {
        fontSize: RFValue(25),
        color: 'white',
    }
})

export default HomeViewController


//TODO
// Custom detail view
// custom scan & show QR view