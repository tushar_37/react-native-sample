import React from 'react'
import { View, StyleSheet, Text, TouchableOpacity, Image, TextInput, KeyboardAvoidingView } from 'react-native'

import Constants from 'expo-constants';

import profileStyles from '../Profile/ProfileStyleSheet'
import EditField from '../../childComponent/EditField'
import UserView from '../../childComponent/UserView'

const SettingsViewController = (props) => {
    return (

        <KeyboardAvoidingView style={profileStyles.screen}>

            < View style={profileStyles.workingBox} >

                <View style={profileStyles.userProfileView}>

                    <View style={{ flexDirection: 'row' }}>

                        <View style={{ flex: 0.85 }}>

                        </View>

                        <TouchableOpacity
                            style={profileStyles.editTouchView}>
                            <Text style={{ fontSize: 22, color: 'white' }}>Edit</Text>
                        </TouchableOpacity>

                    </View>

                    <UserView
                        imageSource={require('../../assets/Hritik.jpeg')}
                        userName="Hritik Roshan"
                        userEmail="hritik.roshan@hrx.com"
                    />

                </View>

                <EditField fieldTittle={'First Name'} placeholder="your first name" fieldValue={'Hritik'} />

                <EditField fieldTittle={'Last Name'} placeholder="your last name" fieldValue={'Roshan'} />

                <EditField fieldTittle={'Email'} placeholder="your email id" fieldValue={'hritik.roshan@hrx.com'} />

            </View>
        </KeyboardAvoidingView >
    )
}



export default SettingsViewController
