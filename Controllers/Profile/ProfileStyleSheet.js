import React from 'react'
import { StyleSheet } from 'react-native'

import Constants from 'expo-constants';


const profileStyles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: '#ffffff',
        alignItems: 'center'
    },
    workingBox: {
        flex: 1,
        marginTop: Constants.statusBarHeight,
        width: '95%',
        backgroundColor: '#ffffff',
        borderRadius: 10,
    },
    userProfileView: {
        flex: 0.50,
        width: '100%',
        backgroundColor: '#ebeff5',
        borderRadius: 10
    },
    editTouchView: {
        flex: 0.15,
        backgroundColor: '#597fbd',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
        padding: 5,
        margin: 10
    },
    userImage: {
        width: '25%',
        height: '35%',
        borderRadius: 30,
        margin: 10,
        justifyContent: 'center'
    },

    userName: {
        fontSize: 30,
    },
    profileFieldTittle: {
        fontSize: 26,
        flex: 0.5
    },
    editTextInput: {
        flex: 0.5,
        fontSize: 22,
        borderRadius: 10,
        // borderWidth: 1,
        padding: 10,

    }
})


export default profileStyles