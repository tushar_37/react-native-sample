
import { StyleSheet } from 'react-native';


const styles = StyleSheet.create({
    item: {
        backgroundColor: "#597fbd",
        padding: 10,
        borderRadius: 15,
        marginVertical: 8,
        height: 180,
        justifyContent: 'space-between',
        alignItems: 'flex-start'
    },
    cardTittle: {
        color: 'white',
        fontSize: 28,
    },
    cardDetails: {
        color: 'white',
        fontSize: 16,
    },
    seprator:{ backgroundColor: 'white', height: 2, width: '100%' }
})


export default styles