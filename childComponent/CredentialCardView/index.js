import React from 'react'
import { Text, TouchableOpacity, View, } from 'react-native';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import styles from './style'

const CredentialCardView = ({ tittle, subTittle, onPress }) => {
    return (
        <TouchableOpacity
            style={styles.item}

            onPress={onPress}>

            <FontAwesome5 name={'address-card'} color={'white'} size={40} />

            <View style={styles.seprator} />

            <Text style={styles.cardTittle}>{tittle}</Text>

            <Text style={styles.cardDetails}>{subTittle}</Text>
        </TouchableOpacity>
    )
}

export default CredentialCardView