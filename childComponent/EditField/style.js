
import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
    editView: {
        flex: 0.10,
        backgroundColor: '#ebeff5',
        borderRadius: 10,
        marginTop: 10,
        padding: 10
    },
    profileFieldTittle: {
        fontSize: 26,
        flex: 0.5
    },
    editTextInput: {
        flex: 0.5,
        fontSize: 22,
        borderRadius: 10,
        padding: 10,
    }
})

export default styles