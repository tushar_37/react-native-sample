
import React from 'react'
import { View, Text, TextInput } from 'react-native'
import styles from './style';


const EditField = ({ placeholder, fieldTittle, fieldValue }) => {
    return (
        <View style={styles.editView}>
            <Text style={styles.profileFieldTittle}>{fieldTittle}</Text>
            <TextInput style={styles.editTextInput} placeholder={placeholder}>{fieldValue}</TextInput>
        </View>
    )
}

export default EditField