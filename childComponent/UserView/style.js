import { StyleSheet } from 'react-native'
const styles = StyleSheet.create({
    userView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    userImage: {
        width: '25%',
        height: '35%',
        borderRadius: 30,
        margin: 10,
        justifyContent: 'center'
    },
    userName: {
        fontSize: 30,
    }
})

export default styles