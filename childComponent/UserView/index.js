import React from 'react'
import { View, Text, StyleSheet, Image } from 'react-native'
import styles from './style';

const UserView = ({ imageSource, userName, userEmail }) => {
    return (
        <View style={styles.userView}>
            <Image style={styles.userImage} source={imageSource} />
            <Text style={styles.userName}>{userName}</Text>
            <Text>{userEmail}</Text>
        </View >
    )
}

export default UserView