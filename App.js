import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { NavigationContainer } from '@react-navigation/native';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'

import HomeViewController from './Controllers/HomeViewController'
import ConnectionsViewController from './Controllers/ConnectionsViewController'
import CredentialsViewController from './Controllers/Credentials/CredentialsViewController'
import ProfileViewController from './Controllers/Profile/ProfileViewController'
import SettingsViewController from './Controllers/SettingsViewController'
import { createStackNavigator } from '@react-navigation/stack';

const Tab = createBottomTabNavigator()

export default function App() {



  const Stack = createStackNavigator();


  const HomeTabStack = () => {
    return (
      <Stack.Navigator>
        <Stack.Screen name="Home" component={HomeViewController} options={{ headerShown: false }} />
        <Stack.Screen name="Settings" component={SettingsViewController} options={{ headerShown: false }} />
      </Stack.Navigator>
    )
  }


  return (
    <NavigationContainer>
      <Tab.Navigator
        screenOptions={({ route }) => ({
          tabBarIcon: ({ focused, size, color }) => {
            let iconName;
            if (route.name === 'Home') {
              iconName = 'home'
            } else if (route.name === 'Connections') {
              iconName = 'user-friends'
            } else if (route.name === 'Credentials') {
              iconName = 'address-card'
            } else if (route.name === 'Profile') {
              iconName = 'user-circle'
            }
            size = focused ? 28 : 16
            color = focused ? '#335e9e' : 'black'
            return (
              <FontAwesome5 name={iconName} size={size} color={color} />
            )
          }
        })}
      >
        <Tab.Screen name="Home" component={HomeTabStack} />
        <Tab.Screen name="Connections" component={ConnectionsViewController} />
        <Tab.Screen name="Credentials" component={CredentialsViewController} />
        <Tab.Screen name="Profile" component={ProfileViewController} />
      </Tab.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

//TODO
// Nested navigation Section 6